##### UPDATES

#### v1.1:
- added a bore for a screw to adjust the tooth backlash of the Titan. The bore is meant to take an M2 hot melt insert (I've used an M2xD3.6xL4.0 insert) and an M2x8 DIN912 / ISO4762 bolt.
  
I've had the problem that, when tightening the top lid bolts on the Titan (Aero), due to too tight tolerances on the bores in the housing, the Titan was pushed back against the adjustment rotation and the gears had way too much clearance. With this modification, the M2 bolt can be pressed against the bottom of the Titan housing to adjust the tooth backlash and the housing will stay in that position when tightening the top lid bolts.
If you didn't encounter that problem, you can just ignore that feature and don't need to add the insert or bolt.

- rounded off the edges of the bores in the mount surface, so slight overextrusion won't result in overhangs that prevent the stepper and/or titan to sit flat on the surfaces.
