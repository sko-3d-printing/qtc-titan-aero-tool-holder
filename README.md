QTC Titan Aero tool holder
==========================

This is a Titan Aero tool holder for the great Quick Tool Change system by ProperPrinting ( https://www.thingiverse.com/thing:3369444 )

The goal was to make it as compact as possible. To fit the tool you might need to make a small cutout to your mount to clear the Titan Aero Heatsink. For reference, I already did that on my Remix of the Quick Tool Change mount for the X5SA Pro which you can find here: https://gitlab.com/sko-3d-printing/qtc-x5sa-pro-mount

This is still a work in progress!
It already works as it is, but e.g. I still want to add a DB12 connector system to it as well as a tool fan which might need mounting brackets on the tool holder. So it still might change quite a bit!


###### Please read the CHANGELOG.md for a list of Updates
